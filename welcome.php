<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{
		$this->data['hasil'] = $this->model_data->getUser('tb_mustahik');
		$this->load->view('welcome_message', $this->data);
	}

	public function form_input()
	{
		$this->load->view('form-input');
	}

	public function insert()
	{
		$nik = $_POST['nik'];
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$no_telp = $_POST['no_telp'];
		$jns_kelamin = $_POST['jns_kelamin'];
		$status = $_POST['status'];

		$data = array('nik' => $nik, 'nama' => $nama, 'alamat' => $alamat, 'no_telp' => $no_telp, 'jns_kelamin' => $jns_kelamin, 'status' => $status, );
		
		$tambah = $this->model_data->tambahData('tb_mustahik',$data);
		if ($tambah > 0)
		{
			echo "<script>alert('Data Berhasil Disimpan'); document.location.href='http://localhost/APDM/index.php'</script>\n";
		}
		else
		{
			echo "<script>alert('Data Gagal Disimpan'); document.location.href='http://localhost/APDM/index.php'</script>\n";
		}
	}

	public function hapus($nik)
	{
		$hapus = $this->model_data->hapusData('tb_mustahik',$nik);

		if ($hapus > 0)
		{
			echo "<script>alert('Data Berhasil Dihapus'); document.location.href='http://localhost/APDM/index.php'</script>\n";
		}
		else
		{
			echo "<script>alert('Data Gagal Dihapus'); document.location.href='http://localhost/APDM/index.php'</script>\n";
		}

	}

	public function form_edit($nik)
	{
		$this->data['dataEdit'] = $this->model_data->dataEdit('tb_mustahik',$nik);
		$this->load->view('form-edit', $this->data);
	}

	public function edit($nik)
	{
		$nik = $_POST['nik'];
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$no_telp = $_POST['no_telp'];
		$jns_kelamin = $_POST['jns_kelamin'];
		$status = $_POST['status'];

		$data = array('nik' => $nik, 'nama' => $nama, 'alamat' => $alamat, 'no_telp' => $no_telp, 'jns_kelamin' => $jns_kelamin, 'status' => $status, );
		
		$edit = $this->model_data->editData('tb_mustahik',$data,$nik);
		if ($edit > 0)
		{
			echo "<script>alert('Data Berhasil Diubah'); document.location.href='http://localhost/APDM/index.php'</script>\n";
		}
		else
		{
			echo "<script>alert('Data Gagal Diubah'); document.location.href='http://localhost/APDM/index.php'</script>\n";
		}
	}
}
