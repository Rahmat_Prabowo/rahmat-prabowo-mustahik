<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>App Pencatatan Data Mustahik</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Data Mustahik</h1>

	<div id="body">
		<a href="<?php echo site_url('welcome/form_input') ?>">Tambah Data</a>
		<table border="1" >
			<thead>
				<tr align="center">
					<td><b>NIK</b></td>
					<td><b>Nama Mustahik</b></td>
					<td><b>Alamat</b></td>
					<td><b>No Telepon</b></td>
					<td><b>Jenis Kelamin</b></td>
					<td><b>Status</b></td>
					<td><b>Pengaturan</b></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach($hasil as $r){ ?>
				<tr align="center">
					<td><?php echo $r['nik'] ?></td>
					<td><?php echo $r['nama'] ?></td>
					<td><?php echo $r['alamat'] ?></td>
					<td><?php echo $r['no_telp'] ?></td>
					<td><?php echo $r['jns_kelamin'] ?></td>
					<td><?php echo $r['status'] ?></td>
					<td>
						<a href="<?php echo site_url('welcome/form_edit/'.$r['nik']) ?>">Ubah Data</a> ||
						<a href="<?php echo site_url('welcome/hapus/'.$r['nik']) ?>" onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data ?')">Hapus Data</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<p class="footer"><strong>Rahmat Prabowo</strong> @Test Recruitment BAZNAS. 2018</p>
</div>

</body>
</html>